import os,csv
import requests
from flask import Flask, session,render_template,request
from flask_session import Session

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

app = Flask(__name__)
os.environ["FLASK_DEBUG"]="TRUE"
os.environ["DATABASE_URL"]="postgres://xbuggalmxbxrru:fe2c6ba902945bc97c3a91a06f72077c8483e2c0554dc101090020f758570cfb@ec2-54-247-72-30.eu-west-1.compute.amazonaws.com:5432/d5qscpcni3dbpn"
os.environ["FLASK_APP"]="app.py"
# Check for environment variable
if not os.getenv("DATABASE_URL"):
    raise RuntimeError("DATABASE_URL is not set")

# Configure session to use filesystem
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# Set up database
engine = create_engine(os.getenv("DATABASE_URL"))
db = scoped_session(sessionmaker(bind=engine))

email=''
password=''
name=''
reviews=[]
books=[]
# APP ROUTES
@app.route("/")
def index():

    return render_template("index.html",name=name,reviews=reviews,books=books)

@app.route("/signup")
def Signup():
    return render_template("signup.html")



@app.route("/login")
def login():
   
    return render_template("login.html")

# SIGNUP FORM
@app.route("/form" , methods=["POST"])
def form():
    name=request.form['name']
    mobile=request.form['mobile']
    dob=request.form['dob']
    gender=request.form['gender']
    email=request.form['email']
    password=request.form['password']
    
    db.execute("INSERT INTO users (name,mobile,dob,gender,email,password) VALUES (:name, :mobile, :dob,:gender,:email,:password)", {"name":name, "mobile":mobile, "dob":dob,"gender":gender,"email":email,"password":password})  
    db.commit()
    return render_template("index.html")

#LOGIN AND DATA RETRIVING
@app.route("/" , methods=["POST"])
def loginform():
    global email
    email=request.form['email']
    global password
    password=request.form['password']
    user=db.execute("SELECT * FROM users WHERE email=:email AND password=:password",{"email":email, "password":password}).fetchone()
    global reviews
    global books
    reviews= db.execute("SELECT * FROM reviews WHERE name=:name",{"name":user.name}).fetchall()
    for review in reviews:
       books.append(db.execute("SELECT * FROM books WHERE isbn=:isbn",{"isbn":review.isbn}).fetchone())
    db.commit()
    global name
    name=user.name
    return render_template("index.html",name=user.name,reviews=reviews,books=books)


# REVIEW SUBMIT FORM
@app.route("/" , methods=["POST"])
def review():
    isbn=request.form['isbn']
    
    review=request.form['review']
    name=request.form['name']
    books=[]
    db.execute("INSERT INTO reviews (name,review,isbn) VALUES (:name,:review,:isbn)",{"name":name, "review":review, "isbn":isbn})
    reviews= db.execute("SELECT * FROM reviews WHERE name=:name",{"name":name}).fetchall()    
    for review in reviews:
        books.append(db.execute("SELECT * FROM books WHERE isbn=:isbn",{"isbn":review.isbn}).fetchone())
    db.commit()
    return render_template("index.html",name=name,reviews=reviews,books=books)

@app.route("/profile")
def profile():
    user=db.execute("SELECT * FROM users WHERE email=:email AND password=:password",{"email":email, "password":password}).fetchone()
    
    db.commit()
    print(user,email,password)
    
    return render_template("profile.html",user=user)    
@app.route("/" , methods=["POST"])
def form1():
    name=request.form['name']
    mobile=request.form['mobile']
    dob=request.form['dob']
    gender=request.form['gender']
    email=request.form['email']
    password=request.form['password']
    
    db.execute("UPDATE users SET name=:name,mobile=:mobile,dob=:dob,gender=:gender,password=:password WHERE email=:email", {"name":name, "mobile":mobile, "dob":dob,"gender":gender,"email":email,"password":password})  
    user=db.execute("SELECT * FROM users WHERE email=:email AND password=:password",{"email":email, "password":password}).fetchone()
    db.commit()
    return render_template("profile.html",user=user)

if __name__ == "__main__":
    app.run(debug=True)
    with app.app_context():
        index()

res = requests.get("https://www.goodreads.com/book/review_counts.json", params={"key": "pZvk20rG45mM1VLWitqYGg", "isbns": "9781632168146"})
print(res.json())